import urllib
import tarfile
import os
import shutil
import pydicom
import sys




def download_url_to_file(url):
	f = urllib.URLopener()
	file_name = url.split('/')[-1]
	f.retrieve(url, file_name)
	return file_name

def extract_files(tgz_path, dest_path):
	f = tarfile.open(tgz_path, 'r:gz')
	f.extractall(path=dest_path)

def download_and_extract_to_dir(url):
	url = url
	print url
	downloaded_file = download_url_to_file(url)
	print downloaded_file
	dcm_dir = downloaded_file+'_all_files'
	print dcm_dir
	extract_files(downloaded_file, dcm_dir)
	print 'extracted'
	return dcm_dir





def arrange_file_hierarcy(dcm_dir, hierarcy_dest):
	dcm_files = os.listdir(dcm_dir)
	if not os.path.exists(hierarcy_dest):
		os.mkdir(hierarcy_dest)
	for f in dcm_files:
		file_name = os.path.join(dcm_dir, f)
		dcm = pydicom.dcmread(file_name)
		name = dcm.PatientName
		patient_dir = os.path.join(hierarcy_dest, name)
		if not os.path.exists(patient_dir):
			os.mkdir(patient_dir)
		study = dcm.StudyInstanceUID
		study_dir = os.path.join(patient_dir, study)
		if not os.path.exists(study_dir):
			os.mkdir(study_dir)
		series = dcm.SeriesInstanceUID
		series_dir = os.path.join(study_dir, series)
		if not os.path.exists(series_dir):
			os.mkdir(series_dir)
		shutil.copy(file_name, series_dir)



def get_hospital_data(dcm):
	hospital = dcm.InstitutionName
	return hospital

def get_patient_data(dcm):
	patient_data = {"Name": dcm.PatientName, "Age": dcm.PatientAge, "Sex": dcm.PatientSex}
	return patient_data

def get_tag_data(dcm, tag):
	if tag in dcm:
		tag_data = dcm[tag]
	else:
		return None
	return tag_data.value

def get_all_data(dcm_dir, data_type, tag=None):
	names = os.listdir(dcm_dir)
	all_data = []
	for d in names:
		dcm_file = dcm_dir+"/"+d
		dcm = pydicom.dcmread(dcm_file)
		if data_type == 'patient':
			data = get_patient_data(dcm)
		if data_type == 'hospital':
			data = get_hospital_data(dcm)
		if data_type == 'tag':
			data = get_tag_data(dcm, tag)
		if data not in all_data:
			all_data.append(data)
	
	return all_data



def print_all_patient_data(all_patient_data):
	for p in all_patient_data:
		print p["Name"], "   ",p["Age"],"   ", p["Sex"]

def get_all_patient_dirs(all_patient_data, hierarcy_dest):
	all_patient_dirs = []
	for patient in all_patient_data:
		p = patient['Name']
		patient_dir = os.path.join(hierarcy_dest, p)
		patient_dirs = {}
		patient_dirs["Patient"] = patient_dir
		patient_dirs['Study'] = []
		patient_dirs['Series'] = []
		for root, dirs, files in os.walk(patient_dir):
			for name in dirs:
				ddir = os.path.join(root, name)
				# print ddir
				if len(ddir.split('\\')) == 3:
					patient_dirs['Study'].append(ddir)
				if len(ddir.split('\\')) == 4:
					patient_dirs['Series'].append(ddir)
		all_patient_dirs.append(patient_dirs)
	return all_patient_dirs





url = sys.argv[1]
hierarcy_dest = "patients"

dcm_dir = download_and_extract_to_dir(url)


arrange_files = True
if arrange_files:
	arrange_file_hierarcy(dcm_dir, hierarcy_dest)
	

patient_data = True
if patient_data:
	all_patient_data = get_all_data(dcm_dir, 'patient')
	print_all_patient_data(all_patient_data)

	patient_dirs = False
	if patient_dirs:
		all_patient_dirs = get_all_patient_dirs(all_patient_data, hierarcy_dest)
		

		tag_data = False
		if tag_data:
			for p in all_patient_dirs:
				print p['Patient']
				for series in p['Series']:
					all_tag_data = get_all_data(series, 'tag', tag=0x00080013)
					all_hospital_data = get_all_data(series, 'hospital')
					print series, all_tag_data, all_hospital_data




hospital_data = True
if hospital_data:
	all_hospital_data = get_all_data(dcm_dir, 'hospital')
	print 'Number of hospitals:', str(len(all_hospital_data))
	print all_hospital_data

